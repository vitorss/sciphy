readFileName = 'input/input.dataset'
writeFileName = 'idataselection.dataset'

rFile = open(readFileName,'r+')
lines = rFile.readlines()

wFile = open(writeFileName,'w')
wFile.write("NAME;FASTA_FILE\n")

i = 0
for l in lines:
	i+=1
	wFile.write(str(i) + ";" + l)
