﻿-- Domain data from...
-- idataselection
SELECT *
FROM sciphy.idataselection;

-- odataselection
SELECT *
FROM sciphy.odataselection;

-- Mafft
SELECT *
FROM sciphy.dlmafft as dl, sciphy.omafft as o
WHERE o.dlmafftid = dl.rid;

-- MergeXML
SELECT *
FROM sciphy.dlmafft as dl1, sciphy.dlreadseq as dl2, sciphy.dlraxml1 as dl3, 
sciphy.dlmodelgenerator as dl4, sciphy.dlraxml2 as dl5,sciphy.omergeraxml as o
WHERE o.dlmafftid = dl1.rid
AND dl2.rid = o.dlreadseqid
AND dl3.rid = o.dlraxml1id
AND dl4.rid = o.dlmodelgeneratorid
AND dl5.rid = o.dlraxml2id;

-- Model generator
SELECT *
FROM sciphy.dlmodelgenerator as dl1, sciphy.omodelgenerator as o, sciphy.dlmafft as dl2, 
sciphy.dlreadseq as dl3
WHERE o.dlmodelgeneratorid = dl1.rid
AND o.dlmafftid = dl2.rid
AND o.dlreadseqid = dl3.rid;

-- RAxML 1
SELECT dl.*, dl1.*, dl2.*, dl3.*
FROM sciphy.dlraxml1 as dl, sciphy.oraxml1 as o, sciphy.dlmodelgenerator as dl1, 
sciphy.dlmafft as dl2, sciphy.dlreadseq as dl3
WHERE o.dlraxml1id = dl.rid
AND o.dlmodelgeneratorid = dl1.rid
AND o.dlmafftid = dl2.rid
AND o.dlreadseqid = dl3.rid;

-- RAxML 2
SELECT *
FROM sciphy.dlraxml2 as dl, sciphy.oraxml2 as o
WHERE o.dlraxml2id = dl.rid;

-- RAxML 3
SELECT *
FROM sciphy.dlraxml3 as dl, sciphy.oraxml3 as o, sciphy.dlmafft as dl2, sciphy.dlreadseq as dl3, 
sciphy.dlraxml1 as dl1, sciphy.dlraxml2 as dl4
WHERE o.dlraxml3id = dl.rid
AND o.dlraxml1id = dl1.rid
AND o.dlmafftid = dl2.rid
AND o.dlreadseqid = dl3.rid
AND o.dlraxml2id = dl4.rid;

-- Readseq
SELECT *
FROM sciphy.dlreadseq as dl, sciphy.oreadseq as o, sciphy.dlmafft as dl1
WHERE o.dlreadseqid = dl.rid
AND o.dlmafftid = dl1.rid;


