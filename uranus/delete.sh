#!/bin/bash
rm machines.conf
rm sciphy.o*

cd exp
rm -rf dataselection
rm -rf mafft
rm -rf modelgenerator
rm -rf raxml*
rm -rf merge*
rm -rf readseq
