#!/bin/bash
# python %=WFDIR%/bin/extractor.py %=FASTA_FILE% PA_Raxml3 dlraxml3.data
java -jar %=WFDIR%/bin/SciPhyExtractor.jar raxml3 RAxML_bipartitions.`basename %=FASTA_FILE%`.phylip_tree3.BS_TREE dlraxml3.data