#!/bin/bash
{ time python %=WFDIR%/../bin/extractor.py %=FASTA_FILE% PA_Raxml dlraxml3.data ; } 2>> extraction.log
{ time java -jar %=WFDIR%/../bin/SciPhyExtractor.jar raxml3 RAxML_bipartitions.`basename %=FASTA_FILE%`.phylip_tree3.BS_TREE dlraxml3.data ; } 2>> extraction.log