# cp /Users/vitor/Documents/Repository/SciCumulus/SciCumulus-Vitor/SCSetup/target/SCSetup-1.0-SNAPSHOT-jar-with-dependencies.jar scc2/SCSetup.jar
# cp /Users/vitor/Documents/Repository/SciCumulus/SciCumulus-Vitor/SCCore/target/SCCore-1.8-jar-with-dependencies.jar scc2/SCCore.jar

echo "\n\n\n\n\n\n\n\n\n\n"
clear

echo "Deleting experiment files..."
./delete.sh

echo "Restoring provenance database..."
./restore.sh
psql -U scc -d scc2-sciphy -a -f script.sql

echo "Executing scientific workflow..."
./execute.sh
