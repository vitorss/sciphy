#!/bin/sh
#set -x

if [ $# -lt 2 ]; then
  echo "Usage: runmpj.sh <conf_file> <jar_file>[OR]<class_file>";
  echo "       For e.g. runmpj.sh ../conf/mpj2.conf ../lib/test.jar";
  echo "       For e.g. runmpj.sh mpj.conf hello.World";
  exit 127
fi

conf=$1
lines=`cat $conf  | egrep -v "#" | egrep "@"`
dir=`pwd`
name=$2
xml=$3
count=0

backslash2slash() {
    echo $1 | sed 's/\\/\//g'
}

CLASSPATH_SEPARATOR=":"
case "`uname`" in
  CYGWIN*) 
    MPJ_HOME=`backslash2slash $MPJ_HOME`
    CLASSPATH=`backslash2slash $CLASSPATH`

    CLASSPATH_SEPARATOR=";"
    ;;
esac


for i in `echo $lines`; do 

  host=`echo $i | cut -d "@" -f 1`
  rank=`echo $i | cut -d "@" -f 3`    

  case "$name" in
    *.jar )
	  echo "rsh visousa@$host java -jar $name $xml $rank; rm -f $conf; &"
      rsh visousa@$host "export MPJ_HOME=$MPJ_HOME; export PATH=$PATH:$MPJ_HOME/bin:$JAVA_HOME/bin; export LD_LIBRARY_PATH=$LD_LIBRARY_PATH; cd $MPJ_HOME; \
          killall java; java -jar $name $xml $rank; rm -f $conf;" &

      ;;

    * )
	  echo "rsh $host java $name $count $rank; rm -f $conf; &"
      rsh $host "ssh -N silva@service0 -L 5432:146.164.31.200:5432; export MPJ_HOME=$MPJ_HOME; export PATH=$PATH:$MPJ_HOME/bin:$JAVA_HOME/bin; export LD_LIBRARY_PATH=$LD_LIBRARY_PATH; cd $MPJ_HOME; \
          java $name $count $rank; rm -f $conf;" &

      ;;
  esac

  

  count=`expr $count + 1`

done

while [ -f $conf ]
do
sleep 10
done





