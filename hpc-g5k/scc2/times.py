#!/usr/local/bin/env python
from __future__ import print_function
import psycopg2, time, sys

dbname = sys.argv[1]
conn_string = "host='localhost' dbname='" + dbname + "' user='visousa' password='visousa' port='5432'"

QUERY_ACT_STATUS="""
SELECT tagexec, ( select round(cast (extract(epoch from (max(a.endtime)-min(a.starttime))/60) AS numeric),1) as ElapsedTime
from eworkflow w, eactivity c, eactivation a
where c.actid = a.actid 
and ew.ewkfid = w.ewkfid
and w.ewkfid = c.wkfid ) as time
from eworkflow as ew 
order by tagexec;
"""

def restart_line():
    sys.stdout.write('\r')
    sys.stdout.flush()

def task():
    try:
        # get a connection, if a connect cannot be made an exception will be raised here
        conn = psycopg2.connect(conn_string)
 
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        cursor = conn.cursor()
 
        # execute our Query
        query = QUERY_ACT_STATUS
        cursor.execute(query)
        # print cursor.fetchall()
        
        txt = str(cursor.fetchall())
        
        txt = txt.replace("[","")
        txt = txt.replace("]","")
        txt = txt.replace("Decimal","")
        txt = txt.replace("('","'")
        lines = txt.split("), ")
        
        for line in lines:
        	line = line.replace(")","")
        	line = line.replace("'","")
	      	print(line)

    except Exception as e: 
        print(e)

if __name__=="__main__": 
    print("\nConnecting to database:\t%s" % (conn_string))
    print("Workflow tag;Time (minutes)")
    task()
    # main(sys.argv[1])    
