#!/usr/bin/python
import os

def validatePhylogeneticTreeFile(self, input):
    """
    validatePhylogeneticTreeFile - Method for tree file verification
                                   Responsible for the PHYLOGENETIC_TREE_VALID tag

    """
    
    return {"PHYLOGENETIC_TREE_VALID":"TRUE"}
