#!/bin/bash
{ time python %=WFDIR%/../bin/extractor.py %=FASTA_FILE% PA_Raxml1 dlraxml1.data ; } 2>> extraction.log
{ time java -jar %=WFDIR%/../bin/SciPhyExtractor.jar raxml RAxML_info.`basename %=FASTA_FILE%`.phylip_raxml_tree1.singleTree dlraxml1.data ; } 2>> extraction.log

sleep 5