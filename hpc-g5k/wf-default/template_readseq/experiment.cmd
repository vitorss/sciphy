#!/bin/bash
java -jar %=WFDIR%/../bin/readseq.jar -all -f=12 %=MAFFT_FILE% -o %=FASTA_FILE%.phylip

python %=WFDIR%/../bin/extractor.py `basename %=FASTA_FILE%` PA_Readseq ERelation.txt

sleep 5