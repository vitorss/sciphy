#!/bin/bash
python %=WFDIR%/../bin/execute_raxml.py . %=PHYLIP% %=MG% 100 4 1
# python %=WFDIR%/../bin/execute_raxml.py . %=PHYLIP% %=MG% 4 4 1

python %=WFDIR%/../bin/extractor.py `basename %=FASTA_FILE%` PA_Raxml1 ERelation.txt

java -jar %=WFDIR%/../bin/SciPhyExtractor.jar raxml RAxML_info.`basename %=FASTA_FILE%`.phylip_raxml_tree1.singleTree ERelation.txt

sleep 5