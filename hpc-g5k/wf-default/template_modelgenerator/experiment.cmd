#!/bin/bash
java -jar %=WFDIR%/../bin/modelgenerator.jar %=FASTA_FILE%.phylip 6 > %=FASTA_FILE%.mg
# java -jar %=WFDIR%/../bin/modelgenerator.jar %=FASTA_FILE%.phylip 1 > %=FASTA_FILE%.mg
python %=WFDIR%/../bin/clean_modelgenerator.py %=FASTA_FILE%.mg

python %=WFDIR%/../bin/extractor.py `basename %=FASTA_FILE%` PA_Modelgenerator ERelation.txt

sleep 5