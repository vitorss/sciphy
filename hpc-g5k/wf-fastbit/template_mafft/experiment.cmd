#!/bin/bash
perl %=WFDIR%/../bin/numberFasta.pl %=FASTA_FILE% > `basename %=FASTA_FILE%`.fastaNumbered
/home/visousa/programs/mafft-7.221-with-extensions/core/mafft `basename %=FASTA_FILE%`.fastaNumbered > `basename %=FASTA_FILE%`.mafft

python %=WFDIR%/../bin/extractor.py `basename %=FASTA_FILE%` PA_Mafft ERelation.txt

sleep 5