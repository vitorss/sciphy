#!/bin/bash
{ time python %=WFDIR%/../bin/extractor.py %=FASTA_FILE% PA_Readseq dlreadseq.data ; } 2>> extraction.log
{ time java -jar %=WFDIR%/../bin/SciPhyExtractor.jar readseq %=FASTA_FILE%.phylip dlreadseq.data ; } 2>> extraction.log
sleep 5