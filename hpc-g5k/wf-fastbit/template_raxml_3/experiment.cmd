#!/bin/bash
python %=WFDIR%/../bin/execute_raxml.py . %=PHYLIP% %=MG% 100 4 3
#python %=WFDIR%/../bin/execute_raxml.py . %=PHYLIP% %=MG% 4 4 3

java -jar %=WFDIR%/../bin/SciPhyTreeGenerator-FastBit.jar RAxML_bipartitions.`basename %=FASTA_FILE%`.phylip_tree3.BS_TREE `basename %=FASTA_FILE%`.tree

sleep 5