#!/bin/bash
rm database.conf
rm machines.conf
rm sciphy.o*

cd exp
rm -rf *.backup
rm -rf dataselection
rm -rf mafft
rm -rf modelgenerator
rm -rf *raxml*
rm -rf readseq
rm -rf storetree

