#!/bin/bash
# environment variables
MESSAGE="Setting environment variables...";echo -e $MESSAGE
ROOT_DIR=/home/visousa/experiment
WORKFLOW_DIR=$ROOT_DIR/sciphy/wf-loading
SCC2_DIR=$ROOT_DIR/sciphy/scc2
ALL_NODES=`cat $OAR_NODE_FILE | uniq`
MASTER=`head -n 1 $OAR_NODE_FILE` # first node
SLAVES=`cat $OAR_NODE_FILE | uniq | tail -n +2` # other nodes
NUM_SLAVES=`cat $OAR_NODE_FILE | uniq | tail -n +2 | wc -l` # number of slaves

# delete.sh
MESSAGE="Deleting files from previous workflow execution...";echo -e $MESSAGE
$WORKFLOW_DIR/delete.sh

# deploy environnement (provided all nodes are in the same cluster)
MESSAGE="Deploying images...";echo -e $MESSAGE
kadeploy3 -a https://api.grid5000.fr/sid/sites/rennes/public/visousa/debian-v1.env -f $OAR_NODE_FILE -k

# generating mpj configuration file
MESSAGE="Generating MPJ configuration file...";echo -e $MESSAGE
# MPI configuration for Chiron
rm $WORKFLOW_DIR/machines.conf
touch $WORKFLOW_DIR/machines.conf
MPJ_CONF=$WORKFLOW_DIR/machines.conf

cat  > $MPJ_CONF << FIM
# Number of processes
`expr $NUM_SLAVES`
# Protocol switch limit 
131072
# MachineName@Port@Rank
FIM
i=0
for NODE in $SLAVES; do
    echo $NODE@20000@$((i++)) >> $MPJ_CONF
    cat ~/.ssh/id_rsa.pub | ssh root@$NODE 'cat >> ~/.ssh/authorized_keys'
    cat ~/.ssh/id_rsa.pub | ssh visousa@$NODE 'cat >> ~/.ssh/authorized_keys'
done
MESSAGE="machines.conf file was created.";echo -e $MESSAGE

MESSAGE="Setting database management system...";echo -e $MESSAGE
DB_SERVER=$WORKFLOW_DIR/database.conf
rm $DB_SERVER
touch $DB_SERVER
echo $MASTER >> $DB_SERVER
MESSAGE="database.conf file was created.";echo -e $MESSAGE

# ChangeXML.jar
MESSAGE="Configuring XML file with database information...";echo -e $MESSAGE
java -jar $SCC2_DIR/ChangeXML.jar $WORKFLOW_DIR/SCC.xml $DB_SERVER 
# PostgreSQL service on main node
MESSAGE="Initializing PostgreSQL...";echo -e $MESSAGE
ssh root@$MASTER /etc/init.d/postgresql start
# SCSetup
MESSAGE="Restoring conceptual workflow...";echo -e $MESSAGE
ssh visousa@$MASTER $WORKFLOW_DIR/restore.sh $WORKFLOW_DIR/SCC.xml.change
scp $WORKFLOW_DIR/script.sql root@$MASTER:/var/lib/postgresql
ssh root@$MASTER psql -U visousa -d scc-2 -a -f /var/lib/postgresql/script.sql
# SCCore + disptacher.sh
MESSAGE="Submiting workflow execution...";echo -e $MESSAGE
ssh visousa@$MASTER $SCC2_DIR/dispatcher.sh $WORKFLOW_DIR/machines.conf $SCC2_DIR/SCCore-2.0.jar $WORKFLOW_DIR/SCC.xml.change
# backup
MESSAGE="Backing up database...";echo -e $MESSAGE
ssh visousa@$MASTER pg_dump scc-2 > $WORKFLOW_DIR/exp/scc-sciphy-loading.backup
# query processing
MESSAGE="Getting workflow execution time...";echo -e $MESSAGE
ssh visousa@$MASTER python $SCC2_DIR/times.py scc-2
