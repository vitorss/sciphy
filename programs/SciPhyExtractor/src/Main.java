/**
 *
 * @author vitor
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(args.length >= 1){
            String activity = args[0];
            if(activity.equals("raxml") && args.length == 3){
                Extractor.raxml(args[1],args[2]);
            }else if(activity.equals("modelgenerator") && args.length == 3){
                Extractor.modelgenerator(args[1],args[2]);
            }else if(activity.equals("readseq") && args.length == 3){
                Extractor.readseq(args[1],args[2]);
            }else if(activity.equals("raxml3") && args.length == 3){
                Extractor.raxml3(args[1],args[2]);
            }
        }
    }
    
}
