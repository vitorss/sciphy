
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author vitor
 */
public class Extractor {

    static void raxml3(String filePath, String outputFile) {
        try {
            String taxonFile = filePath.split("\\.")[1];
            final String dir = System.getProperty("user.dir") + "/";
            filePath = dir + filePath;

            String phytree = "";
            HashMap<String, String> taxons = new HashMap<String, String>();

            FileReader fr = new FileReader(taxonFile + ".fasta.fastaNumbered");
            BufferedReader br = new BufferedReader(fr);

            String number = "";
            String realTaxon = "";
            while (br.ready()) {
                String line = br.readLine();
                if (line.startsWith(">")) {
                    if (!number.isEmpty()) {
                        taxons.put(number, realTaxon);
                    }

                    number = line.replace(">", "");
                } else {
                    realTaxon += line;
                }
            }

            if (!number.isEmpty()) {
                taxons.put(number, realTaxon);
            }

            FileReader file = new FileReader(filePath);
            BufferedReader buff = new BufferedReader(file);
            double minBootstrap = 100.0;

            if (buff.ready()) {
                String tree = buff.readLine();
                phytree += tree;
                int findIndex = tree.indexOf(")");
                tree = tree.substring(findIndex + 1);

                double currentValue = 101.0;

                while (findIndex != -1) {
                    int commonIndex = tree.indexOf(":");
                    if (commonIndex != -1) {
                        currentValue = Double.valueOf(tree.substring(0, commonIndex));
                        tree = tree.substring(commonIndex + 1);

                        if (currentValue < minBootstrap) {
                            minBootstrap = currentValue;
                        }

                        findIndex = tree.indexOf(")");
                        tree = tree.substring(findIndex + 1);
                    } else {
                        findIndex = -1;
                    }
                }
            }

            for (String key : taxons.keySet()) {
                phytree = phytree.replace("(" + key + ")", "(" + taxons.get(key) + ")");
                phytree = phytree.replace("(" + key + ":", "(" + taxons.get(key) + ":");
                phytree = phytree.replace("," + key + ":", "(" + taxons.get(key) + ":");
            }

            File f = new File(outputFile);
            f.createNewFile();
            FileReader relFile = new FileReader(f);
            BufferedReader relBuff = new BufferedReader(relFile);

            String relation = "";
            String rLine = relBuff.readLine();
            if (rLine != null) {
                relation = rLine + ";TREE;MINBOOTSTRAP;PHYTREE\n";

                rLine = relBuff.readLine();
                while (rLine != null) {
                    if (!rLine.isEmpty()) {
                        relation += rLine + ";" + filePath + ";" + minBootstrap + ";" + phytree + "\n";
                    }

                    rLine = relBuff.readLine();
                }
            } else {
                relation = "TREE;MINBOOTSTRAP;PHYTREE\n";
                relation += filePath + ";" + minBootstrap + ";" + phytree + "\n";
            }

            FileWriter fstream = new FileWriter(outputFile);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(relation);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void raxml(String filePath, String outputFile) {
        try {
            String bestScore = "ERROR";
            String template = "Final GAMMA-based Score of best tree ";

            FileReader file = new FileReader(filePath);
            BufferedReader buff = new BufferedReader(file);
            String line = buff.readLine();

            while (line != null) {
                if (line.startsWith(template)) {
                    bestScore = line.replaceAll(template, "");
                    break;
                }

                line = buff.readLine();
            }
            buff.close();
            file.close();

            template = "Likelihood   : ";
            if (bestScore.equals("ERROR")) {
                file = new FileReader(filePath);
                buff = new BufferedReader(file);
                line = buff.readLine();

                while (line != null) {
                    if (line.startsWith(template)) {
                        bestScore = line.replaceAll(template, "");
                        break;
                    }

                    line = buff.readLine();
                }
                buff.close();
                file.close();
            }

            File f = new File(outputFile);
            f.createNewFile();
            FileReader relFile = new FileReader(f);
            BufferedReader relBuff = new BufferedReader(relFile);

            String relation = "";
            String rLine = relBuff.readLine();
            if (rLine != null) {
                relation = rLine + ";BESTSCORE\n";

                rLine = relBuff.readLine();
                while (rLine != null) {
                    if (!rLine.isEmpty()) {
                        relation += rLine + ";" + bestScore + "\n";
                    }

                    rLine = relBuff.readLine();
                }

            } else {
                relation = "BESTSCORE\n";
                relation += bestScore + "\n";
            }

            FileWriter fstream = new FileWriter(outputFile);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(relation);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void modelgenerator(String filePath, String outputFile) {
        try {
            FileReader file = new FileReader(filePath);
            BufferedReader buff = new BufferedReader(file);

            ArrayList<ArrayList<String>> criteria = new ArrayList<ArrayList<String>>();
            String line = buff.readLine();
            String template = "****Akaike Information Criterion ";
            String modelTemplate = "Model Selected: ";
            String probabilityTemplate = "-lnL = ";

            while (line != null) {
                String model = "";
                String probability = "";
                if (line.startsWith(template)) {
                    line = buff.readLine();
                    while (line != null) {
                        if (line.startsWith(modelTemplate)) {
                            model = line.replaceAll(modelTemplate, "");
                        } else if (line.startsWith(probabilityTemplate)) {
                            probability = line.replaceAll(probabilityTemplate, "");
                        }

                        if (!model.isEmpty() && !probability.isEmpty()) {
                            ArrayList<String> criterion = new ArrayList<String>();
                            criterion.add(model);
                            criterion.add(probability);
                            criteria.add(criterion);
                            break;
                        }

                        line = buff.readLine();
                    }
                }

                line = buff.readLine();
            }

            FileReader relFile = new FileReader(outputFile);
            BufferedReader relBuff = new BufferedReader(relFile);

            String relation = "";
            String rLine = relBuff.readLine();
            if (rLine != null) {
                relation = rLine;
                for (int i = 1; i <= criteria.size(); i++) {
                    relation += ";MODEL" + i + ";" + "PROB" + i;
                }
                relation += "\n";
            }

            rLine = relBuff.readLine();
            while (rLine != null) {
                if (!rLine.isEmpty()) {
                    relation += rLine;
                    for (int i = 0; i < criteria.size(); i++) {
                        relation += ";" + criteria.get(i).get(0) + ";" + criteria.get(i).get(1);
                    }
                    relation += "\n";
                }

                rLine = relBuff.readLine();
            }

            FileWriter fstream = new FileWriter(outputFile);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(relation);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void readseq(String filePath, String outputFile) {
        try {
            FileReader file = new FileReader(filePath);
            BufferedReader buff = new BufferedReader(file);

            String aligns = "";
            String length = "";

            String line = buff.readLine();
            if (line != null) {
                String[] cols = line.trim().split(" ");
                aligns = cols[0];
                length = cols[1];
            }

            File f = new File(outputFile);
            String relation = "";
            if (f.exists()) {
                FileReader relFile = new FileReader(outputFile);
                BufferedReader relBuff = new BufferedReader(relFile);

                String rLine = relBuff.readLine();
                if (rLine != null) {
                    relation = rLine + ";NUM_ALIGNS;LENGTH\n";
                }

                rLine = relBuff.readLine();
                while (rLine != null) {
                    if (!rLine.isEmpty()) {
                        relation += rLine + ";" + aligns + ";" + length + "\n";
                    }

                    rLine = relBuff.readLine();
                }
            } else {
                relation = "NUM_ALIGNS;LENGTH\n";
                relation += aligns + ";" + length + "\n";
            }

            FileWriter fstream = new FileWriter(outputFile);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(relation);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
