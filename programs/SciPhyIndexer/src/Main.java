/**
 *
 * @author vitor
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(args.length >= 1){
            String activity = args[0];
            if(activity.equals("raxml3") && args.length == 3){
                Extractor.raxml3(args[1],args[2]);
            }
        }
    }
    
}
