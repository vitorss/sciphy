/**
 *
 * @author vitor
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(args.length >= 2){
            Extractor.raxml3(args[0],args[1]);
        }
    }
    
}
