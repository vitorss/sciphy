
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author vitor
 */
public class Extractor {

    static void raxml3(String filePath, String outputFile) {
        try {
            String taxonFile = filePath.split("\\.")[1];
            final String dir = System.getProperty("user.dir") + "/";
            filePath = dir + filePath;

            String phytree = "";
            HashMap<String, String> taxons = new HashMap<String, String>();

            FileReader fr = new FileReader(taxonFile + ".fasta.fastaNumbered");
            BufferedReader br = new BufferedReader(fr);

            String number = "";
            String realTaxon = "";
            while (br.ready()) {
                String line = br.readLine();
                if (line.startsWith(">")) {
                    if (!number.isEmpty()) {
                        taxons.put(number, realTaxon);
                    }

                    number = line.replace(">", "");
                } else {
                    realTaxon += line;
                }
            }

            if (!number.isEmpty()) {
                taxons.put(number, realTaxon);
            }

            FileReader file = new FileReader(filePath);
            BufferedReader buff = new BufferedReader(file);

            if (buff.ready()) {
                String tree = buff.readLine();
                phytree += tree;
                int findIndex = tree.indexOf(")");
                tree = tree.substring(findIndex + 1);

                double currentValue = 101.0;

                while (findIndex != -1) {
                    int commonIndex = tree.indexOf(":");
                    if (commonIndex != -1) {
                        currentValue = Double.valueOf(tree.substring(0, commonIndex));
                        tree = tree.substring(commonIndex + 1);

                        findIndex = tree.indexOf(")");
                        tree = tree.substring(findIndex + 1);
                    } else {
                        findIndex = -1;
                    }
                }
            }

            for (String key : taxons.keySet()) {
                phytree = phytree.replace("(" + key + ")", "(" + taxons.get(key) + ")");
                phytree = phytree.replace("(" + key + ":", "(" + taxons.get(key) + ":");
                phytree = phytree.replace("," + key + ":", "(" + taxons.get(key) + ":");
            }

            File f = new File(outputFile);
            f.createNewFile();
            
//            original
            String relation = phytree + "\n";
//            fastbit
//            String relation = "PHYTREE\n" + phytree + "\n";

            FileWriter fstream = new FileWriter(outputFile);
            try (BufferedWriter out = new BufferedWriter(fstream)) {
                out.write(relation);
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }

}
