package FastaSelection;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitor
 */
public class Main {
    
    public static String name = "";
    public static String fastFilePath = "";
    public static String outputFile = "";
    public static int minAlignments = 3;
    public static int maxAlignments = 10;
    
    public static void main(String[] args) {
        if(args.length >= 3){
            name = args[0];
            fastFilePath = args[1];
            outputFile = args[2];
            minAlignments = Integer.parseInt(args[3]);
            maxAlignments = Integer.parseInt(args[4]);
            
            validateFastaFile();
        }
    }

    private static void validateFastaFile() {
        int fastaCounter = 0;
        
        FileReader file = null;
        try {
            file = new FileReader(fastFilePath);
            BufferedReader buff = new BufferedReader(file);
            
            String line = buff.readLine();
            while (line != null) {
                if(line.startsWith(">")){
                    fastaCounter++;
                }
                line = buff.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                file.close();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(fastaCounter > minAlignments && fastaCounter < maxAlignments){
            FileWriter writer = null;
            try {
                writer = new FileWriter(outputFile);
                BufferedWriter buffer = new BufferedWriter(writer);
                buffer.write("NAME;FASTA_FILE\n");
                buffer.write(name + ";" + fastFilePath);
                buffer.flush();
                buffer.close();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
    
}
