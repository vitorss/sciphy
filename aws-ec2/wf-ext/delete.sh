#!/bin/bash
rm machines.conf
rm sciphy.o*
rm sar_*
cd exp
rm -rf dataselection
rm -rf mafft
rm -rf modelgenerator
rm -rf *raxml*
rm -rf readseq
rm -rf storetree
cd ..
rm -rf scc2.backup
