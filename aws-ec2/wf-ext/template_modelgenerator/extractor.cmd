#!/bin/bash
{ time python %=WFDIR%/../bin/extractor.py %=FASTA_FILE% PA_Modelgenerator dlmodelgenerator.data ; } 2>> extraction.log
{ time java -jar %=WFDIR%/../bin/SciPhyExtractor.jar modelgenerator modelgenerator0.out dlmodelgenerator.data ; } 2>> extraction.log